package uz.itpu;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.elasticsearch.hadoop.cfg.ConfigurationOptions;

import java.util.List;

// add this JVM Option in IDEA IDE: "--add-exports java.base/sun.nio.ch=ALL-UNNAMED"

/**
 * Reads CSV data from a directory and writes it to Elasticsearch.
 */
public class SparkCsvToElastic {

    private static final String INPUT_DIRECTORY = "src/main/resources/receipt_restaurants";
    private static final String CSV_HEADER = "id,franchise_id,franchise_name,restaurant_franchise_id,country,city,lat,lng,receipt_id,total_cost,discount,date_time";
    private static final String ES_INDEX = "restaurants/data";

    public static void main(String[] args) {
        startStreaming();
    }

    /**
     * Reads CSV data from the specified directory and writes it to Elasticsearch.
     */
    public static void startStreaming() {
        SparkConf conf = new SparkConf().setMaster("local[2]").setAppName("SparkCsvToElastic");
        JavaStreamingContext streamingContext = new JavaStreamingContext(conf, Durations.seconds(5));

        SparkSession spark = SparkSession.builder()
                .config(ConfigurationOptions.ES_NODES, "172.31.0.2")
                .config(ConfigurationOptions.ES_PORT, "9200")
                .config(ConfigurationOptions.ES_NODES_WAN_ONLY, "true")
                .appName("RestaurantReceiptStreamingJob")
                .master("local[*]")
                .getOrCreate();

        StructType schema = createSchema();

        JavaDStream<String> lines = streamingContext.textFileStream(INPUT_DIRECTORY);

        JavaDStream<String[]> csvInputData = lines
                .filter(line -> !line.startsWith(CSV_HEADER))
                .map(line -> line.split(","));

        csvInputData.foreachRDD(rdd -> {
            List<Row> rowList = rdd.map((Function<String[], Row>) record -> RowFactory.create((Object[]) record)).collect();

            if (!rowList.isEmpty()) {
                Dataset<Row> csvDataFrame = spark.createDataFrame(rowList, schema);

                csvDataFrame.write()
                        .format("org.elasticsearch.spark.sql")
                        .option("es.resource", ES_INDEX)
                        .mode(SaveMode.Append)
                        .save();

                csvDataFrame.show();
            }
        });

        streamingContext.start();
        try {
            streamingContext.awaitTermination();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } finally {
            // Close resources
            streamingContext.close();
            spark.close();
        }
    }

    /**
     * Creates the schema for the CSV data.
     *
     * @return The schema.
     */
    private static StructType createSchema() {
        String[] columnNames = CSV_HEADER.split(",");
        StructField[] fields = new StructField[columnNames.length];
        for (int i = 0; i < columnNames.length; i++) {
            fields[i] = DataTypes.createStructField(columnNames[i], DataTypes.StringType, true);
        }
        return new StructType(fields);
    }
}
