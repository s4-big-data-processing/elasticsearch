# Practical Task: Elasticsearch

Author: [Ziyodulla Baykhanov](https://www.linkedin.com/in/ziyodulla-baykhanov/)

Date: 20th of May, 2024

Email: [ziyodulla_baykhanov@student.itpu.uz](mailto:ziyodulla_baykhanov@student.itpu.uz)

## Creating a docker-compose file to run Elasticsearch, Logstash, and Kibana

### Creating a virtual Docker network

Change the directory to the location where the `docker-compose.yml` file is located:

```bash
cd elasticsearch
```

![image](screenshots/Screenshot01.png)

List all Docker containers to verify that no containers are running:

```bash
docker ps -a
```

![image](screenshots/Screenshot02.png)

### Deploying with docker compose

Start the Docker containers using the following command:

```bash
docker compose up -d
```

![image](screenshots/Screenshot03.png)

List all Docker containers to verify that the containers are running:

```bash
docker ps
```

![image](screenshots/Screenshot04.png)

### Copying the Logstash configuration file to the Logstash container

Inspect the container to find the gateway's IP address and mark the IP address, which in my case is `172.31.0.2`:

```bash
docker inspect es
```

![image](screenshots/Screenshot05.png)

Copy the Logstash configuration file to the Logstash container:

```bash
docker cp /elasticsearch/logstash.conf log:/usr/share/logstash/pipeline/logstash.conf
```

Checking the Logstash configuration file

```bash
docker exec -it log cat /usr/share/logstash/pipeline/logstash.conf
```

![image](screenshots/Screenshot06.png)

Restart the container to apply the changes.

Verify that the Elasticsearch container is running by accessing the following URL in a web browser: <http://localhost:9200>

![image](screenshots/Screenshot07.png)

Verify that the Logstash container is running by accessing the following URL in a web browser: <http://localhost:9600>

![image](screenshots/Screenshot08.png)

Verify that the Kibana container is running by accessing the following URLs in a web browser: <http://localhost:5601/api/status> and <http://localhost:5601/status>

![image](screenshots/Screenshot09.png)
![image](screenshots/Screenshot10.png)

## Creating the Spark Streaming job that reads source dataset files and publishes data into Elastic

Folder structure:

```markdown
elasticsearch/
├── README.md
├── docker-compose.yml
├── logstash.conf
├── pom.xml
|── .gitignore
└── src/
    ├── main/
    │   └── java/
    │       └── uz/
    │           └── itpu/
    │               └── SparkCsvToElastic.java
    └── resources/
        └── receipt_restaurants/
            └── part-files.csv
```

Example of `part-00000-533b6aa9-2b29-4da8-8554-715668de302b-c000.csv` file content:

```csv
id,franchise_id,franchise_name,restaurant_franchise_id,country,city,lat,lng,receipt_id,total_cost,discount,date_time
188978561075,52,The Red Door,5034,AT,Vienna,48.215,16.376,56df62bf-f7e7-47ff-8800-475bf46262cf,17.40,0.15,2022-09-05T17:32:50.000Z
214748364857,58,A Tasty Bite,23242,FR,Paris,48.866,2.332,f3ed7e84-f3c7-46e7-b855-f6def62911bb,17.12,0.0,2021-10-01T18:53:23.000Z
266287972391,40,Crimson Cafe,70700,IT,Milan,45.473,9.191,4cbfe14a-77ab-489e-aeb8-192931ad493a,13.90,0.0,2022-09-07T07:02:40.000Z
```

Example of `SparkCsvToElastic.java` file content:

```java
    /**
     * Reads CSV data from the specified directory and writes it to Elasticsearch.
     */
    public static void startStreaming() {
        SparkConf conf = new SparkConf().setMaster("local[2]").setAppName("SparkCsvToElastic");
        JavaStreamingContext streamingContext = new JavaStreamingContext(conf, Durations.seconds(5));

        SparkSession spark = SparkSession.builder()
                .config(ConfigurationOptions.ES_NODES, "172.31.0.2")
                .config(ConfigurationOptions.ES_PORT, "9200")
                .appName("SparkCsvToElastic")
                .master("local[*]")
                .getOrCreate();

        // rest of the code
    }
```

![image](screenshots/Screenshot11.png)
![image](screenshots/Screenshot12.png)

## Running Kibana and creating a dashboard showing restaurants by country on the line chart

Access the Kibana Index Management by accessing the following URL in a web browser: <http://localhost:5601/app/management/data/index_management/indices>

![image](screenshots/Screenshot13.png)

I created a dashboard in Kibana to visualize time series data, showing the count of occurrences by country over time. Starting in the Visualization section, I selected a line chart and chose the appropriate index pattern containing the time and country fields.

I configured the X-axis to display time using a date histogram and set the Y-axis to count the entries. To differentiate the counts by country, I added a sub-bucket with a terms aggregation on the country field.

![image](screenshots/Screenshot14.png)
After customizing the visualization's appearance and settings, I saved it and incorporated it into a dashboard, where it can be easily accessed and analyzed.

![image](screenshots/Screenshot15.png)

The first chart displays the count of restaurants by country over **days**. The line chart shows the number of restaurants in each country, with the x-axis representing the date and the y-axis representing the count. The different colors represent the countries, making it easy to distinguish between them.

![image](screenshots/Screenshot16.png)

The second chart displays the count of restaurants by country over **weeks**. The line chart shows the number of restaurants in each country, with the x-axis representing the month and the y-axis representing the count. The different colors represent the countries, making it easy to distinguish between them.
